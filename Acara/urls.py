from django.urls import path
from sepeda import views
app_name = 'Acara'

urlpatterns = [
	path('', views.listacara, name = 'acara')
]
