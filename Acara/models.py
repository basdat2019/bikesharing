from django.db import models

# Create your models here.
class Penugasan(models.Model):
    no_ktp = models.CharField(primary_key=True, max_length=50)
    waktu_mulai = models.CharField(max_length=300)
    waktu_selesai = models.IntegerField(blank=True, null=True)
    id_stasiun = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'penugasan'