from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt


def Penugasan(request):
    response={}
    c = connection.cursor()
    c.execute('set search_path to bike_sharing,public')
    c.execute("SELECT nama FROM stasiun")

    result1 = c.fetchall()
    response['penugasan'] = result1
    print(response)
    return render(request, 'createpenugasan.html',response)
