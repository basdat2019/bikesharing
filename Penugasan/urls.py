from django.urls import path
from sepeda import views
app_name = 'Penugasan'

urlpatterns = [
	path('', views.listpenugasan, name = 'penugasan')
]
