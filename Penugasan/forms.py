from django import forms
from .models import Stasiun


class StasiunForm(forms.Form):
    no_ktp = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    waktu_mulai = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    waktu_selesai = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), required=False)
    id_stasiun = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), required=False)
