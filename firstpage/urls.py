from django.urls import path
from firstpage import views

urlpatterns = [
    path('', views.index, name='index'),
    path('sign-up', views.signup, name='sign-up'),
    path('login', views.login, name='login'),
    path('arahinkesini', views.arahinkesini, name='ea'),
    path('logout', views.logout, name='logout'),
]
