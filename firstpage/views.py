from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
cursor = connection.cursor()
def index(request):
	if 'ktp' in request.session:
		return render(request, 'dashboard.html', {'role':request.session['role']})
	else:
		return render(request,'login-register.html')

def signup(request):
	if request.method == "GET":
		return render(request,'signup.html')
	elif request.method == "POST":
		print(request.POST["role"])


def login(request):

	return render(request,'login.html')

def logout(request):
	del request.session['ktp']
	return redirect('/')

def arahinkesini(request):
	ktp = request.POST["ktp"]
	email = request.POST["email"]

	ketemu = False
	cursor.execute("SELECT * FROM ANGGOTA NATURAL JOIN PERSON WHERE ktp='" + ktp +  "' and email='"+email+"'")
	hasil = dictfetchall(cursor)
	if len(hasil) > 0:
		role = 'anggota'
		ketemu = True

	else:
		cursor.execute("SELECT * FROM PETUGAS NATURAL JOIN PERSON WHERE ktp='" + ktp +  "' and email='"+email+"'")
		hasil = dictfetchall(cursor)
		if len(hasil) > 0:
			role = 'petugas'
			ketemu = True
		else:
			cursor.execute("SELECT * FROM PERSON WHERE ktp='" + ktp +  "' and email='"+email+"'")
			hasil = dictfetchall(cursor)
			if len(hasil) > 0:
				role = 'admin'
				ketemu = True

	if ketemu:
		request.session['ktp'] = ktp
		request.session['role'] = role
		return redirect('/')
	else:
		return redirect('/login')

def dictfetchall(cursor): 
	"Returns all rows from a cursor as a dict" 
	desc = cursor.description 
	return [
			dict(zip([col[0] for col in desc], row)) 
			for row in cursor.fetchall() 
	]