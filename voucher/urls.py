from django.urls import path
from sepeda import views
app_name = 'sepeda'

urlpatterns = [
	path('', views.listsepeda, name = 'sepeda'),
	path('/createsepeda',views.sepeda,name='createsepeda'),
	path('/updatesepeda',views.updatesepeda,name='updatesepeda'),
	path('/postsepeda',views.postsepeda, name = 'postsepeda'),
    path('/deletesepeda',views.deletesepeda, name = 'deletesepeda'),
    path('/detailupdate',views.detailupdate, name = 'detailupdate'),
	path('/sortSepedaDESC/',views.sortSepedaDESC, name = 'sortSepedaDESC'),
	path('/sortSepedaASC/', views.sortSepedaASC, name = 'sortSepedaASC'),
]
