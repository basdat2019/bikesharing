from django.db import models

# Create your models here.
class Stasiun(models.Model):
    nama = models.CharField(primary_key=True, max_length=50)
    alamat = models.CharField(max_length=300)
    latitude = models.IntegerField(blank=True, null=True)
    longitude = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stasiun'
