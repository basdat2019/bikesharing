from django import forms
from .models import Stasiun


class StasiunForm(forms.Form):
    nama = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    alamat = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-field flex-column'}))
    latitude = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), required=False)
    longitude = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-field flex-column'}), required=False)
